<?php

namespace App\Controller;

use App\Repository\MoviesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MoviesController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(MoviesRepository $repo)
    {
        return $this->render('/movies/index.html.twig', [
            'movies' => $repo->findAll()
        ]);
    }
}
