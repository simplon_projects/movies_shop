<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/user", name="user")
     */
    public function index(UserRepository $repo)
    {
        return $this->render('user/userHome.html.twig', [
            'users' => $repo->findAll(),
        ]);
    }
    
    /**
     * @Route("/user/cart", name="user_cart")
     */
    public function userCart()
    {
        return $this->render('user/cart.html.twig');
    }
}
