<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CartRepository")
 */
class Cart
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Movies")
     */
    private $cartContent;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->cartContent = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Movies[]
     */
    public function getCartContent(): Collection
    {
        return $this->cartContent;
    }

    public function addCartContent(Movies $cartContent): self
    {
        if (!$this->cartContent->contains($cartContent)) {
            $this->cartContent[] = $cartContent;
        }

        return $this;
    }

    public function removeCartContent(Movies $cartContent): self
    {
        if ($this->cartContent->contains($cartContent)) {
            $this->cartContent->removeElement($cartContent);
        }

        return $this;
    }
}
