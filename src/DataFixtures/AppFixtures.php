<?php

namespace App\DataFixtures;

use App\Entity\Movies;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i < 11; $i++) {
            $movie = new Movies();

            $movie->setName('Movie N°' . $i)
                ->setDirector('Director' . $i)
                ->setDescription('This movie has been seen ' . $i . ' times')
                ->setPrice(20);
            $manager->persist($movie);
        
        }
        $manager->flush();
    }
}
